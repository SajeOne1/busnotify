import setuptools

setuptools.setup(
    name="busnotify",
    version="1.0.0",
    author="Shane Brown",
    author_email="contact@shane-brown.ca",
    description="Simple bus arrival notification app",
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPL-3.0",
        "Operating System :: OS Independent",
    ],
    scripts=['busnotify/busnotify'],
    python_requires='>=3.6',
)
